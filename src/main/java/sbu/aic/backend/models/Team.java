package sbu.aic.backend.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.swing.*;
import java.awt.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="teams")
@Data
@ToString(exclude = {"sentMatchInvites","teamInvites","members","codes","receivedMatchInvites"})
@EqualsAndHashCode(exclude={"sentMatchInvites","teamInvites","members","codes","receivedMatchInvites"})
public class Team {

    public Team() {
        this.members = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer score;
    private String name;
    private String image;

    @OneToMany(fetch=FetchType.EAGER,mappedBy = "team")
    @JsonManagedReference
    Set<CodeFiles> codes = new HashSet<>();


    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "team")
    Set<User> members;

    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "senderTeam")
    Set<TeamInvite> teamInvites;

    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "senderTeam")
    Set<MatchInvite> sentMatchInvites;
    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "receiverTeam")
    Set<MatchInvite> receivedMatchInvites;

    @JsonManagedReference
    @ManyToMany (cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(
            name = "participation",
            joinColumns = @JoinColumn(name = "team_id"),
            inverseJoinColumns = @JoinColumn(name = "match_id"))
    Set<Match> matches;


    public void addMember(User user){
        members.add(user);
    }
}
