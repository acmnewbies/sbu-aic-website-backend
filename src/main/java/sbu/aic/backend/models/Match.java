package sbu.aic.backend.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.awt.*;
import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="matchInvites")
@Data
@ToString(exclude = {"teams","codes"})
@EqualsAndHashCode(exclude={"teams","codes"})
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date date;
    private String winnerName;
    private Integer result;
    private String mapName;
    private String logFile;
    private Boolean friendly;


    @JsonBackReference
    @ManyToMany(fetch=FetchType.EAGER,mappedBy = "matches",cascade=CascadeType.ALL)
    Set<Team> teams;

    @JsonBackReference
    @ManyToMany(fetch=FetchType.EAGER,mappedBy = "matches",cascade=CascadeType.ALL)
    Set<CodeFiles> codes;


}
