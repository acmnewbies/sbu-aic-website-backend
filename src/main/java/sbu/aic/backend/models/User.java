package sbu.aic.backend.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
@Data
@ToString(exclude = {"confirmationToken", "forgotToken","sentTeamInvites" , "receivedTeamInvite", "team"})
@EqualsAndHashCode(exclude={"confirmationToken", "forgotToken","sentTeamInvites" , "receivedTeamInvite", "team"})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String email;
    private String password;
    private String fullName;
    private String userName;
    private String organization;

    private Boolean enabled;

    public User() {
        super();
        this.enabled = false;
        this.sentTeamInvites = new HashSet<>();
        this.receivedTeamInvite = new HashSet<>();
    }

    @JsonBackReference
    @ManyToOne
    private Team team;

    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "sender")
    Set<TeamInvite> sentTeamInvites;

    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "receiver")
    Set<TeamInvite> receivedTeamInvite ;

    @JsonManagedReference
    @OneToOne(mappedBy = "user")
    ConfirmationToken confirmationToken;

    @JsonBackReference
    @OneToOne(mappedBy = "user")
    ForgotToken forgotToken;
}
