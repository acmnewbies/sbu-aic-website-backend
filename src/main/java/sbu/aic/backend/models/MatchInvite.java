package sbu.aic.backend.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="matchInvites")
@Data
@ToString(exclude = {"senderTeam","receiverTeam"})
@EqualsAndHashCode(exclude={"senderTeam","receiverTeam"})
public class MatchInvite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date date;
    private Boolean done;
    private Boolean accepted;


    @JsonBackReference
    @ManyToOne
    private Team senderTeam;

    @JsonBackReference
    @ManyToOne
    private Team receiverTeam;
}
