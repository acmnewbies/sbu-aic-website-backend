package sbu.aic.backend.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name="codeFiles")
@Data
@ToString(exclude = {"matches"})
@EqualsAndHashCode(exclude={"matches"})
public class CodeFiles {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String url;//file address in server

    @JsonBackReference
    @ManyToOne
    private Team team;

    @JsonManagedReference
    @ManyToMany(cascade= CascadeType.ALL, fetch= FetchType.EAGER)
    @JoinTable(
            name = "usedCodes",
            joinColumns = @JoinColumn(name = "code_id"),
            inverseJoinColumns = @JoinColumn(name = "match_id"))
    Set<Match> matches;
}
