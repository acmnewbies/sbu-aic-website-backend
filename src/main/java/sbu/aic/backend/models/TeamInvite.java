package sbu.aic.backend.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="teamInvites")
@Data
@ToString(exclude = {"sender","receiver","senderTeam"})
@EqualsAndHashCode(exclude={"sender","receiver","senderTeam"})
public class TeamInvite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date date;
    private Boolean accepted;


    @JsonBackReference
    @ManyToOne
    Team senderTeam;

    @JsonBackReference
    @ManyToOne
    User receiver;

    @JsonBackReference
    @ManyToOne
    User sender;



}
