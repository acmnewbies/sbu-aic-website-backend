package sbu.aic.backend.controllers;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sbu.aic.backend.models.ForgotToken;
import sbu.aic.backend.models.User;
import sbu.aic.backend.services.email.EmailService;
import sbu.aic.backend.services.forgotToken.ForgotTokenService;
import sbu.aic.backend.services.user.UserService;
import sbu.aic.backend.utill.assemblers.EmailAssembler;
import sbu.aic.backend.utill.constants.EmailMessages;
import sbu.aic.backend.utill.constants.URLS;
import sbu.aic.backend.utill.exceptions.NotFoundExceptions.TokenNotFoundException;
import sbu.aic.backend.utill.exceptions.NotFoundExceptions.UserNotFoundException;

import java.util.Map;

@RestController
public class PasswordController {
    private final UserService userService;
    private final EmailService emailService;
    private final SecurityController securityController;
    private final ForgotTokenService forgotTokenService;
    private final EmailAssembler emailAssembler;

    public PasswordController(UserService userService, EmailService emailService, SecurityController securityController, ForgotTokenService forgotTokenService, EmailAssembler emailAssembler) {
        this.userService = userService;
        this.emailService = emailService;
        this.securityController = securityController;
        this.forgotTokenService = forgotTokenService;
        this.emailAssembler = emailAssembler;
    }

    @PostMapping("/forgot")
    public void resetPasswordViaEmail(@RequestBody Map<String, String> params) {
        User user = getUserFromEmail(params);
        ForgotToken forgotToken = generateForgotToken(user);
        sendPasswordRecoveryEmailToUser(user, forgotToken);
    }

    //token nadare hanoz va az body e request email ro barmidarim
    //json yek map az string be obj ...
    private User getUserFromEmail(@RequestBody Map<String, String> params) {
        String userEmail = params.get("email");
        return userService.findByEmail(userEmail)
                .orElseThrow(() -> new UserNotFoundException(userEmail));
    }

    private ForgotToken generateForgotToken(User user) {
        ForgotToken forgotToken = new ForgotToken();
        forgotToken.setUser(user);
        forgotTokenService.save(forgotToken);
        return forgotToken;
    }

    private void sendPasswordRecoveryEmailToUser(User user, ForgotToken forgotToken) {
        String message = EmailMessages.RECOVER_MESSAGE + forgotToken.getForgotToken();
        SimpleMailMessage mail = emailAssembler.createMail(user.getEmail(), EmailMessages.RECOVER_SUBJECT, message);
        emailService.sendSimpleEmail(mail);
    }


    //TODO validate password
    @PostMapping("/reset")
    public String reset(@RequestParam Map<String, String> params) {
        ForgotToken forgotToken = forgotTokenService.findByForgotToken(params.get("token")).orElseThrow(TokenNotFoundException::new);
        String newPassword = params.get("password");
        changeUserPasswordViaForgotToken(newPassword, forgotToken);
        return EmailMessages.RECOVER_COMPLETE;
    }

    private void changeUserPasswordViaForgotToken(String password, ForgotToken forgotToken) {
        User user = forgotToken.getUser();
        saveUser(password, user);
        saveToken(forgotToken);
    }

    private void saveUser(String password, User user) {
        user.setPassword(securityController.bCryptPasswordEncoder.encode(password));
        userService.saveUser(user);
    }

    private void saveToken(ForgotToken forgotToken) {
        forgotToken.setForgotToken("Activated");
        forgotTokenService.save(forgotToken);
    }

}