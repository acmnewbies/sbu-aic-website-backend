package sbu.aic.backend.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import sbu.aic.backend.domains.matchInvitation.MatchInviteInputDTO;
import sbu.aic.backend.domains.team.TeamInputDTO;
import sbu.aic.backend.domains.teamInvitation.TeamInviteInputDTO;
import sbu.aic.backend.services.Team.TeamService;
import sbu.aic.backend.services.user.UserService;

@RestController
public class TeamController {

    TeamService teamService;

    public TeamController(UserService userService , TeamService teamService) {
        this.teamService = teamService;
    }

    @PostMapping(path = "/createTeam")
    public void createTeam(@RequestBody TeamInputDTO teamDTO, @RequestHeader("Authorization") String token){
        teamService.saveNewTeam(teamDTO , token);

    }
    @PostMapping(path = "/sendTeamInvitation")
    public void sendInvitation(@RequestBody TeamInviteInputDTO teamInviteInputDTO , @RequestHeader("Authorization") String token){
        teamService.sendInvitation(teamInviteInputDTO , token);
    }

    @PostMapping(path = "/acceptTeamInvitation")
    public void acceptTeamInvitation(@RequestBody TeamInviteInputDTO teamInviteInputDTO, @RequestHeader("Authorization") String token){
        teamService.acceptTeamInvitation(teamInviteInputDTO , token);

    }

    @PostMapping(path = "/rejectTeamInvitation")
    public void rejectTeamInvitation(@RequestBody TeamInviteInputDTO teamInviteInputDTO, @RequestHeader("Authorization") String token){
        teamService.rejectTeamInvitation(teamInviteInputDTO,token);
    }
    @PostMapping(path = "/sendMatchInvitation")
    public void sendMatchInvitation(@RequestBody MatchInviteInputDTO matchInviteInputDTO , @RequestHeader("Authorization") String token){
        teamService.sendMatchInvitation(matchInviteInputDTO , token);
    }

}
