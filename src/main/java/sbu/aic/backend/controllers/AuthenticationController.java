package sbu.aic.backend.controllers;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import sbu.aic.backend.services.UserDetailsServiceImpl;
import sbu.aic.backend.domains.authentication.AuthenticationResponse;
import sbu.aic.backend.domains.authentication.LoginDTO;
import sbu.aic.backend.utill.jwt.TokenUtil;

@RestController
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final TokenUtil tokenUtil;
    private final UserDetailsService userDetailsService;

    public AuthenticationController(AuthenticationManager authenticationManager, TokenUtil tokenUtil, UserDetailsServiceImpl userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.tokenUtil = tokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @PostMapping("/login")
    public AuthenticationResponse createAuthenticationToken(@RequestBody LoginDTO authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
     //userdatail : username/pass/admin...
        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());

        String token = tokenUtil.generateToken(userDetails);
        return new AuthenticationResponse(token);

    }
//check kardane user pass ba database va check kardane authenticate bodan masalan expired token
    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        }
    }

}
