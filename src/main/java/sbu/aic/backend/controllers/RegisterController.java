package sbu.aic.backend.controllers;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;
import sbu.aic.backend.domains.user.UserInputDTO;
import sbu.aic.backend.domains.user.UserOutputDTO;
import sbu.aic.backend.models.ConfirmationToken;
import sbu.aic.backend.models.User;
import sbu.aic.backend.services.confiramtionToken.ConfirmationTokenService;
import sbu.aic.backend.services.email.EmailService;
import sbu.aic.backend.services.user.UserService;
import sbu.aic.backend.utill.assemblers.EmailAssembler;
import sbu.aic.backend.utill.constants.EmailMessages;
import sbu.aic.backend.utill.exceptions.IllegalInputExceptions.IllegalUserInputException;
import sbu.aic.backend.utill.exceptions.NotFoundExceptions.TokenNotFoundException;


@RestController
public class RegisterController {

    private final UserService userService;
    private final ConfirmationTokenService confirmationTokenService;
    private final EmailService emailService;
    private final EmailAssembler emailAssembler;

    public RegisterController(UserService userService, ConfirmationTokenService confirmationTokenService, EmailService emailService, EmailAssembler emailAssembler) {
        this.userService = userService;
        this.confirmationTokenService = confirmationTokenService;
        this.emailService = emailService;
        this.emailAssembler = emailAssembler;
    }

    //TODO make rest
    @GetMapping(path = "/me")
    public UserOutputDTO getUserInfo(@RequestHeader("Authorization") String token) {
        User user = userService.getUserWithToken(token);
        return UserOutputDTO.fromModel(user);
    }

    @PostMapping(path = "/signup")
    public void addUser(@RequestBody UserInputDTO user) {
        if (isUserValid(user))
            signUserUp(user);
        else
            throw new IllegalUserInputException();

    }

    private void signUserUp(@RequestBody UserInputDTO user) {
        User savedUser = this.userService.addOne(user);
        ConfirmationToken confirmationToken = generateTokenForUser(savedUser);
        sendVerificationEmailToUser(user, confirmationToken);
    }

    private ConfirmationToken generateTokenForUser(User savedUser) {
        ConfirmationToken ct = new ConfirmationToken();
        ct.setUser(savedUser);
        return this.confirmationTokenService.save(ct);
    }

    @GetMapping(path = "/confirmAccount")
    public String confirmAccount(@RequestParam("token") String token) {
        User user = findUserByEmailToken(token);
        user.setEnabled(true);
        userService.saveUser(user);
        return EmailMessages.CONFIRM_COMPLETE;
    }

    private User findUserByEmailToken(@RequestParam("token") String token) {
        ConfirmationToken confirmationToken = confirmationTokenService.findByConfirmationToken(token).orElseThrow(TokenNotFoundException::new);
        User user = confirmationToken.getUser();
        if (user.getEnabled())
            throw new TokenNotFoundException();
        return user;
    }

    private void sendVerificationEmailToUser(@RequestBody UserInputDTO user, ConfirmationToken confirmationToken) {
        String message = EmailMessages.CONFIRM_MESSAGE + confirmationToken.getConfirmationToken();
        SimpleMailMessage mail = emailAssembler.createMail(user.getEmail(), EmailMessages.CONFIRM_SUBJECT, message);
        emailService.sendSimpleEmail(mail);
    }


    //TODO VALIDATE USER
    private boolean isUserValid(UserInputDTO user) {
        return true;
    }

}