package sbu.aic.backend.controllers;

import com.sun.org.apache.regexp.internal.RE;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sbu.aic.backend.domains.team.TeamInputDTO;
import sbu.aic.backend.domains.user.UserOutputDTO;
import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.User;
import sbu.aic.backend.services.user.UserService;

@RestController
public class ProfileController {

    UserService userService;

    public ProfileController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping(path = "/profile")
    public UserOutputDTO getProfileInfo(@RequestHeader("Authorization") String token) {
        User user = userService.getUserWithToken(token);
        return UserOutputDTO.fromModel(user);

    }


}
