package sbu.aic.backend.services.forgotToken;

import sbu.aic.backend.models.ForgotToken;

import java.util.Optional;

public interface ForgotTokenService {
    ForgotToken save(ForgotToken forgotToken);
    Optional<ForgotToken> findByForgotToken(String forgotToken);
}