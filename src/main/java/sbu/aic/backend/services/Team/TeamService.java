package sbu.aic.backend.services.Team;

import sbu.aic.backend.domains.matchInvitation.MatchInviteInputDTO;
import sbu.aic.backend.domains.team.TeamInputDTO;
import sbu.aic.backend.domains.teamInvitation.TeamInviteInputDTO;
import sbu.aic.backend.models.Team;

import java.util.Optional;

public interface TeamService {

    Team saveNewTeam(TeamInputDTO teamInputDTO , String token);
    void sendInvitation(TeamInviteInputDTO teamInviteInputDTO , String token);
    Optional<Team> findByName(String name);
    void acceptTeamInvitation (TeamInviteInputDTO teamInviteInputDTO,String Token);
    void rejectTeamInvitation (TeamInviteInputDTO teamInviteInputDTO,String Token);
    void sendMatchInvitation(MatchInviteInputDTO matchInviteInputDTO, String token);

}
