package sbu.aic.backend.services.Team;

import javassist.NotFoundException;
import org.springframework.stereotype.Service;
import sbu.aic.backend.domains.matchInvitation.MatchInviteInputDTO;
import sbu.aic.backend.domains.team.TeamInputDTO;
import sbu.aic.backend.domains.teamInvitation.TeamInviteInputDTO;
import sbu.aic.backend.models.MatchInvite;
import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.TeamInvite;
import sbu.aic.backend.models.User;
import sbu.aic.backend.repositories.MatchInviteRepository;
import sbu.aic.backend.repositories.TeamInviteRepository;
import sbu.aic.backend.repositories.TeamRepository;
import sbu.aic.backend.repositories.UserRepository;
import sbu.aic.backend.services.user.UserService;
import sbu.aic.backend.utill.exceptions.NotFoundExceptions.UserNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class TeamServiceImpl implements TeamService {

    TeamRepository teamRepository;
    UserService userService;
    TeamInviteRepository teamInviteRepository;
    MatchInviteRepository matchInviteRepository;

    public TeamServiceImpl(TeamRepository teamRepository, UserService userService, TeamInviteRepository teamInviteRepository,MatchInviteRepository matchInviteRepository) {
        this.teamRepository = teamRepository;
        this.userService = userService;
        this.teamInviteRepository = teamInviteRepository;
        this.matchInviteRepository = matchInviteRepository;
    }

    @Override
    public Team saveNewTeam(TeamInputDTO teamInputDTO, String token) {
        User user = userService.getUserWithToken(token);
        Team team = teamInputDTO.toModel();
        user.setTeam(team);
        team.addMember(user);
        return teamRepository.save(team);
    }

    @Override
    public void sendMatchInvitation(MatchInviteInputDTO matchInviteInputDTO, String token) {
        User sender = userService.getUserWithToken(token);
        Team senderTeam = sender.getTeam();
        Team receiverTeam = teamRepository.findByName(matchInviteInputDTO.getreceiverTeam()).orElseThrow(() -> new UserNotFoundException("receiverTeamNotFound"));

        MatchInvite matchInvite = matchInviteInputDTO.toModel(senderTeam, receiverTeam);
        System.out.println(matchInvite.getSenderTeam());

        matchInvite = matchInviteRepository.save(matchInvite);

        manageMatchInviteReceiver(receiverTeam, matchInvite);
        manageMatchInviteSender(senderTeam, matchInvite);
        System.out.println(sender.getTeam().getReceivedMatchInvites());
    }

    @Override
    public void sendInvitation(TeamInviteInputDTO teamInviteInputDTO, String token) {

        User sender = userService.getUserWithToken(token);
        System.out.println("snder id: " + sender.getId());
        String teamName = teamInviteInputDTO.getTeamName();


        if (!teamInviteInputDTO.getTeamName().equals(sender.getTeam().getName()))
            return;

        Team team = teamRepository.findByName(teamName).orElseThrow(() -> new IllegalArgumentException("senderTeam not found"));
        User receiver = userService.findByEmail(teamInviteInputDTO.getReceiver()).orElseThrow(() -> new UserNotFoundException("receiver user not found"));

        System.out.println(receiver.getTeam());
        if (receiver.getTeam() != null)
            return;

        if (teamInviteRepository.existsByReceiverAndSenderTeam(receiver, sender.getTeam()))
            return;


        TeamInvite teamInvite = teamInviteInputDTO.toModel(sender, receiver, team);
        teamInvite = teamInviteRepository.save(teamInvite);

//        team.getTeamInvites().add(teamInvite);
//        teamRepository.save(team);

        manageTeamInviteTeam(team, teamInvite);
        manageTeamInviteReceiver(receiver, teamInvite);
        manageTeamInviteSender(sender, teamInvite);
        System.out.println("ljhalf"+ "   "+sender.getTeam().getMembers().size());
//
//        sender.getSentTeamInvites().add(teamInvite);
//        receiver.getReceivedTeamInvite().add(teamInvite);
//
//        userService.saveUser(sender);
//        userService.saveUser(receiver)

    }


    @Override
    public void acceptTeamInvitation(TeamInviteInputDTO teamInviteInputDTO, String token) {

        User receiver = userService.getUserWithToken(token);
        User sender = userService.findByEmail(teamInviteInputDTO.getSender()).orElseThrow(() -> new IllegalArgumentException("sender not found"));

        String teamName = teamInviteInputDTO.getTeamName();
        Team team = teamRepository.findByName(teamName).orElseThrow(() -> new IllegalArgumentException("team Not Found"));

        addNewTeamMember(team, receiver);

        TeamInvite teamInvite = teamInviteRepository.findBySenderTeamAndReceiverAndSender(team, receiver, sender);
        teamInviteRepository.delete(teamInvite);
        manageAcceptedTeamInviteSender(teamInvite, sender);
        manageAcceptedTeamInviteReceiver(teamInvite, receiver);
//        team.addMember(receiver);
//        receiver.setTeam(team);
//        teamRepository.save(team);

    }


    @Override
    public void rejectTeamInvitation(TeamInviteInputDTO teamInviteInputDTO, String token) {
//        System.out.println("11111");
        User receiver = userService.getUserWithToken(token);
        User sender = userService.findByEmail(teamInviteInputDTO.getSender()).orElseThrow(() -> new IllegalArgumentException("sender not found"));

        String teamName = teamInviteInputDTO.getTeamName();
        Team team = teamRepository.findByName(teamName).orElseThrow(() -> new IllegalArgumentException("team Not Found"));

        TeamInvite teamInvite = teamInviteRepository.findBySenderTeamAndReceiverAndSender(team, receiver, sender);
        teamInviteRepository.delete(teamInvite);
//        System.out.println("2222");
        manageRejectedTeamInviteReceiver(teamInvite, receiver);
//        System.out.println("3333");
        manageRejectedTeamInviteSender(teamInvite, sender);

    }

    @Override
    public Optional<Team> findByName(String name) {
        return teamRepository.findByName(name);
    }

    private void addNewTeamMember(Team team, User receiver) {
        team.addMember(receiver);
        receiver.setTeam(team);
        teamRepository.save(team);
    }

    private void manageTeamInviteReceiver(User receiver, TeamInvite teamInvite) {
        receiver.getReceivedTeamInvite().add(teamInvite);
        userService.saveUser(receiver);

    }

    private void manageTeamInviteSender(User sender, TeamInvite teamInvite) {
        sender.getSentTeamInvites().add(teamInvite);
        userService.saveUser(sender);
        System.out.println("member size : " + sender.getTeam().getMembers().size());

    }

    private void manageTeamInviteTeam(Team team, TeamInvite teamInvite) {
        team.getTeamInvites().add(teamInvite);
        teamRepository.save(team);
    }

    private void manageAcceptedTeamInviteSender(TeamInvite teamInvite, User sender) {
        sender.getSentTeamInvites().remove(teamInvite);
        userService.saveUser(sender);
    }

    private void manageAcceptedTeamInviteReceiver(TeamInvite teamInvite, User receiver) {
        receiver.getReceivedTeamInvite().remove(teamInvite);
        userService.saveUser(receiver);
    }

    private void manageRejectedTeamInviteReceiver(TeamInvite teamInvite, User receiver) {
        System.out.println("receiver teamInvite : " + teamInvite.toString());
        System.out.println(receiver.toString());
        System.out.println("rec size : " + receiver.getReceivedTeamInvite().size());
        receiver.getReceivedTeamInvite().remove(teamInvite);
        System.out.println("rec size : " + receiver.getReceivedTeamInvite().size());
        userService.saveUser(receiver);
//        System.out.println("hi2");
    }

    private void manageRejectedTeamInviteSender(TeamInvite teamInvite, User sender) {
        System.out.println("sender team invite : " + teamInvite.toString());
        sender.getSentTeamInvites().remove(teamInvite);
        System.out.println("sender size : " + sender.getSentTeamInvites().size());
        userService.saveUser(sender);
    }


    private void manageMatchInviteReceiver(Team receiverTeam, MatchInvite matchInvite) {
        receiverTeam.getReceivedMatchInvites().add(matchInvite);
        teamRepository.save(receiverTeam);
    }

    private void manageMatchInviteSender(Team senderTeam, MatchInvite matchInvite) {
        senderTeam.getSentMatchInvites().add(matchInvite);
        teamRepository.save(senderTeam);
    }
}