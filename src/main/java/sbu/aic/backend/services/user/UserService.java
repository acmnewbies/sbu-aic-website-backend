package sbu.aic.backend.services.user;

import sbu.aic.backend.domains.user.UserInputDTO;
import sbu.aic.backend.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> getOne(Long id);
    List<User> getAll();
    User addOne(UserInputDTO user);
    Optional<User> findByEmail(String email);
    User saveUser(User user);
    User getUserWithToken(String token);
}
