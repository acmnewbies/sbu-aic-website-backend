package sbu.aic.backend.services;

import lombok.Getter;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sbu.aic.backend.models.User;
import sbu.aic.backend.repositories.UserRepository;
import sbu.aic.backend.utill.exceptions.NotFoundExceptions.UserNotFoundException;

import java.util.ArrayList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private final UserRepository userRepository;

	@Getter
	private User user;

	public UserDetailsServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		User user = userRepository.findByEmail(email).orElseThrow(()-> new UserNotFoundException(email));
		if (!user.getEnabled()) {
			throw new DisabledException("This user is not activated");
		}

		this.user = user;
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), new ArrayList<>());
	}

}
