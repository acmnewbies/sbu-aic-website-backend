package sbu.aic.backend.services.confiramtionToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sbu.aic.backend.models.ConfirmationToken;
import sbu.aic.backend.repositories.ConfirmationTokenRepository;

import java.util.Optional;

@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    @Autowired
    private ConfirmationTokenRepository repository;

    @Override
    public ConfirmationToken save(ConfirmationToken confirmationToken) {
        return repository.save(confirmationToken);
    }

    @Override
    public Optional<ConfirmationToken> findByConfirmationToken(String confirmationToken) {
        return repository.findByConfirmationToken(confirmationToken);
    }

}
