package sbu.aic.backend.services.confiramtionToken;

import sbu.aic.backend.models.ConfirmationToken;

import java.util.Optional;

public interface ConfirmationTokenService {
    ConfirmationToken save(ConfirmationToken confirmationToken);
    Optional<ConfirmationToken> findByConfirmationToken(String confirmationToken);
}
