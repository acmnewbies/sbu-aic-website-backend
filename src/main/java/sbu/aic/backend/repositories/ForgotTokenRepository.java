package sbu.aic.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sbu.aic.backend.models.ForgotToken;
import sbu.aic.backend.models.User;

import java.util.Optional;

@Repository
public interface ForgotTokenRepository extends CrudRepository<ForgotToken, Long> {
    Optional<ForgotToken> findByForgotToken(String forgotToken);
    Optional<ForgotToken> findByUser(User user);
}
