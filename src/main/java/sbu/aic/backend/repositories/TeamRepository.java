package sbu.aic.backend.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestHeader;
import sbu.aic.backend.models.Team;

import java.util.Optional;

@Repository
public interface TeamRepository extends CrudRepository<Team,Long> {

    Optional<Team> findByName(String name);

}
