package sbu.aic.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.TeamInvite;
import sbu.aic.backend.models.User;

@Repository
public interface TeamInviteRepository extends CrudRepository<TeamInvite , Long> {

public Boolean existsByReceiverAndSenderTeam(User receiver , Team senderTeam);
public TeamInvite findBySenderTeamAndReceiverAndSender(Team senderTeam , User receiver , User sender);
}
