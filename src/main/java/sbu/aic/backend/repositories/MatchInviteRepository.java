package sbu.aic.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sbu.aic.backend.models.MatchInvite;
import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.TeamInvite;
import sbu.aic.backend.models.User;

@Repository
public interface MatchInviteRepository extends CrudRepository<MatchInvite , Long> {


}
