package sbu.aic.backend.filters;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import sbu.aic.backend.services.UserDetailsServiceImpl;
import sbu.aic.backend.utill.exceptions.NotFoundExceptions.TokenNotFoundException;
import sbu.aic.backend.utill.jwt.JwtTokenUtil;
import sbu.aic.backend.utill.jwt.TokenExtractor;
import sbu.aic.backend.utill.jwt.TokenUtil;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFilter extends OncePerRequestFilter {

    private UserDetailsService userDetailsService;
    private TokenUtil tokenUtil;

    @Autowired
    public AuthenticationFilter(UserDetailsServiceImpl userDetailsService, JwtTokenUtil tokenUtil) {
        this.userDetailsService = userDetailsService;
        this.tokenUtil = tokenUtil;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String requestTokenHeader = request.getHeader("Authorization");
        evaluateToken(request, requestTokenHeader);
        chain.doFilter(request, response);
    }

    private void evaluateToken(HttpServletRequest request, String requestTokenHeader) {
        TokenExtractor tokenExtractor = new TokenExtractor(requestTokenHeader, tokenUtil).invoke();

        if (isAuthenticationOk(tokenExtractor.getUsername()))
            authenticate(request, tokenExtractor.getUsername(), tokenExtractor.getJwtToken());
    }

    private void authenticate(HttpServletRequest request, String username, String jwtToken) {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
        if (tokenUtil.validateToken(jwtToken, userDetails))
            evaluateAuthentication(request, userDetails);
    }

    private void evaluateAuthentication(HttpServletRequest request, UserDetails userDetails) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());
        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    private boolean isAuthenticationOk(String username) {
        return username != null && SecurityContextHolder.getContext().getAuthentication() == null;
    }

}