package sbu.aic.backend.domains.teamInvitation;

import org.modelmapper.ModelMapper;
import sbu.aic.backend.domains.team.MemberOutputDTO;
import sbu.aic.backend.domains.team.TeamOutputDTO;
import sbu.aic.backend.domains.user.UserOutputDTO;
import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.TeamInvite;
import sbu.aic.backend.models.User;

public class TeamInviteOutputDTO {


        private Boolean accepted;


        String teamName;

        String receiver;

        String sender;


    public static TeamInviteOutputDTO fromModel(TeamInvite teamInvite) {
        TeamInviteOutputDTO teamInviteOutputDTO = new TeamInviteOutputDTO();
        teamInviteOutputDTO.accepted = teamInvite.getAccepted();
        teamInviteOutputDTO.teamName = teamInvite.getSenderTeam().getName();
        teamInviteOutputDTO.receiver = teamInvite.getReceiver().getEmail();
        teamInviteOutputDTO.sender = teamInvite.getSender().getEmail();
        return teamInviteOutputDTO;

    }
}
