package sbu.aic.backend.domains.teamInvitation;

import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.TeamInvite;
import sbu.aic.backend.models.User;


public class TeamInviteInputDTO {

    private String receiver;
    private String teamName;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    private String sender;

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }


    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }


    public TeamInvite toModel(User sender, User receiver, Team team) {

        TeamInvite teamInvite = new TeamInvite();
        teamInvite.setReceiver(receiver);
        teamInvite.setSender(sender);
        teamInvite.setSenderTeam(team);

        return teamInvite;
    }


}
