package sbu.aic.backend.domains.authentication;

import lombok.Data;

@Data
public class AuthenticationResponse {
	
	private final String token;

}
