package sbu.aic.backend.domains.authentication;

import lombok.Data;

@Data
public class LoginDTO {

	private String email;
	private String password;

}
