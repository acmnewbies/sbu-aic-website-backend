package sbu.aic.backend.domains.team;

import lombok.Data;
import org.modelmapper.ModelMapper;
import sbu.aic.backend.domains.teamInvitation.TeamInviteOutputDTO;
import sbu.aic.backend.domains.user.UserOutputDTO;
import sbu.aic.backend.models.TeamInvite;
import sbu.aic.backend.models.User;

import java.util.HashSet;
import java.util.Set;

@Data
public class MemberOutputDTO {
    private String fullName;
    private String organization;
    private String email;
    private Set<TeamInviteOutputDTO> sentTeamInvites = new HashSet<>();
    private Set<TeamInviteOutputDTO> receivedTeamInvites = new HashSet<>();

    public static MemberOutputDTO fromModel(User user) {
        ModelMapper mapper = new ModelMapper();
        MemberOutputDTO outputDTO = mapper.map(user, MemberOutputDTO.class);
        for (TeamInvite teamInvite: user.getSentTeamInvites()) {
            outputDTO.sentTeamInvites.add(TeamInviteOutputDTO.fromModel(teamInvite));
        }
        for (TeamInvite teamInvite: user.getReceivedTeamInvite()) {
            outputDTO.receivedTeamInvites.add(TeamInviteOutputDTO.fromModel(teamInvite));
        }
        return outputDTO;
    }

}
