package sbu.aic.backend.domains.team;

import lombok.Data;
import org.modelmapper.ModelMapper;
import sbu.aic.backend.models.Team;

@Data
public class TeamInputDTO {

    private String name;

    public  Team toModel() {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(this, Team.class);
    }
}
