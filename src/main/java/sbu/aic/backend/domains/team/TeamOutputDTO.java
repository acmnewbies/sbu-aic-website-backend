package sbu.aic.backend.domains.team;

import lombok.Data;
import org.modelmapper.ModelMapper;
import sbu.aic.backend.domains.user.UserOutputDTO;
import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.User;

import java.util.HashSet;
import java.util.Set;

public class TeamOutputDTO {

    private String name;

    private Set<MemberOutputDTO> members;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MemberOutputDTO> getMembers() {
        return members;
    }

    public void setMembers(Set<MemberOutputDTO> members) {
        this.members = members;
    }

    public TeamOutputDTO() {
        this.members = new HashSet<>();
    }

    public static TeamOutputDTO fromModel(Team team) {
        ModelMapper modelMapper = new ModelMapper();
        TeamOutputDTO teamOutputDTO = modelMapper.map(team, TeamOutputDTO.class);

        //   teamOutputDTO.name = senderTeam.getName();

        for (User user : team.getMembers()
        ) {
            teamOutputDTO.members.add(MemberOutputDTO.fromModel(user));
        }
        return teamOutputDTO;

    }



}

