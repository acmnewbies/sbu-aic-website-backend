package sbu.aic.backend.domains.matchInvitation;

import sbu.aic.backend.models.MatchInvite;
import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.User;

public class MatchInviteInputDTO {

    private String senderTeam;
    private String receiverTeam;

    public String getsenderTeam() {
        return senderTeam;
    }

    public void setsenderTeam(String senderTeam) {
        this.senderTeam = senderTeam;
    }

    public String getreceiverTeam() {
        return receiverTeam;
    }

    public void setreceiverTeam(String receiverTeam) {
        this.receiverTeam = receiverTeam;
    }

   public MatchInvite toModel(Team senderTeam , Team receiverTeam){

        MatchInvite matchInvite = new MatchInvite();
        matchInvite.setReceiverTeam(receiverTeam);
        matchInvite.setSenderTeam(senderTeam);
        return matchInvite;
   }

}
