package sbu.aic.backend.domains.user;

import lombok.Data;
import org.modelmapper.ModelMapper;
import sbu.aic.backend.domains.team.TeamOutputDTO;
import sbu.aic.backend.domains.teamInvitation.TeamInviteOutputDTO;
import sbu.aic.backend.models.Team;
import sbu.aic.backend.models.TeamInvite;
import sbu.aic.backend.models.User;

import java.util.HashSet;
import java.util.Set;


public class UserOutputDTO {

    private String fullName;
    private String organization;
    private String email;
    private TeamOutputDTO team;
    private Set<TeamInviteOutputDTO> sentTeamInvites;
    private Set<TeamInviteOutputDTO> receivedTeamInvites;

    public static UserOutputDTO fromModel(User user) {
        ModelMapper mapper = new ModelMapper();

        UserOutputDTO outputDTO = mapper.map(user, UserOutputDTO.class);
        outputDTO.sentTeamInvites = new HashSet<>();
        outputDTO.receivedTeamInvites = new HashSet<>();
        for (TeamInvite teamInvite: user.getSentTeamInvites()) {
            outputDTO.sentTeamInvites.add(TeamInviteOutputDTO.fromModel(teamInvite));
        }
     //  System.out.println(user.getTeam().getMembers().size());
//        System.out.println(outputDTO.receivedTeamInvites.size());
        for (TeamInvite teamInvite: user.getReceivedTeamInvite()) {
            System.out.println(teamInvite.getSender().getEmail());
            outputDTO.receivedTeamInvites.add(TeamInviteOutputDTO.fromModel(teamInvite));
        }
        if (user.getTeam() != null)
            outputDTO.team = TeamOutputDTO.fromModel(user.getTeam());
        return outputDTO;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TeamOutputDTO getTeam() {
        return team;
    }

    public void setTeam(TeamOutputDTO team) {
        this.team = team;
    }

    public Set<TeamInviteOutputDTO> getSentTeamInvites() {
        return sentTeamInvites;
    }

    public void setSentTeamInvites(Set<TeamInviteOutputDTO> sentTeamInvites) {
        this.sentTeamInvites = sentTeamInvites;
    }

    public Set<TeamInviteOutputDTO> getReceivedTeamInvites() {
        return receivedTeamInvites;
    }

    public void setReceivedTeamInvites(Set<TeamInviteOutputDTO> receivedTeamInvites) {
        this.receivedTeamInvites = receivedTeamInvites;
    }
}
