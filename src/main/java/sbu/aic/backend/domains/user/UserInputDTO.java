package sbu.aic.backend.domains.user;

import lombok.Data;
import org.modelmapper.ModelMapper;
import sbu.aic.backend.models.User;

@Data
public class UserInputDTO {
    private String userName;
    private String fullName;
    private String password;
    private String organization;
    private String email;

    public static User toModel(UserInputDTO user) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(user, User.class);
    }
}