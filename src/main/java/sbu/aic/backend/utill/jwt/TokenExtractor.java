package sbu.aic.backend.utill.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import sbu.aic.backend.utill.exceptions.NotFoundExceptions.TokenNotFoundException;

public class TokenExtractor {
    private String requestTokenHeader;
    private String username;
    private String jwtToken;
    private TokenUtil tokenUtil;
    private final Log logger = LogFactory.getLog(getClass());

    public TokenExtractor(String requestTokenHeader, TokenUtil tokenUtil) {
        this.requestTokenHeader = requestTokenHeader;
        this.username = null;
        this.jwtToken = null;
        this.tokenUtil = tokenUtil;
    }

    public String getUsername() {
        return username;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public TokenExtractor invoke() {
        if (isTokenBearer(requestTokenHeader)) {
            jwtToken = requestTokenHeader.substring(7);
            username = getUserName(jwtToken);
        } else {
            logger.warn("JWT Token does not begin with Bearer String");
        }
        return this;
    }

    private boolean isTokenBearer(String requestTokenHeader) {
        return requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ");
    }

    private String getUserName(String jwtToken) {
        String username = null;
        try {
            username = tokenUtil.getUsernameFromToken(jwtToken).orElseThrow(TokenNotFoundException::new);
        } catch (IllegalArgumentException e) {
            System.out.println("Unable to get JWT Token");
        } catch (ExpiredJwtException e) {
            System.out.println("JWT Token has expired");
        }
        return username;
    }
}