package sbu.aic.backend.utill.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import sbu.aic.backend.services.UserDetailsServiceImpl;
import sbu.aic.backend.domains.authentication.AuthenticationResponse;
import sbu.aic.backend.domains.authentication.LoginDTO;
import sbu.aic.backend.models.User;
import sbu.aic.backend.utill.exceptions.NotFoundExceptions.TokenNotFoundException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Component
public class JwtTokenUtil  implements TokenUtil {

	private static final long serialVersionUID = -2550185165626007488L;
	private static final long JWT_TOKEN_VALIDITY =  30 * 24 * 60 * 60;

	@Value("${jwt.secret}")
	private String secret;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Override
	public Optional<String> getUsernameFromToken(String token) {
		return Optional.of(getClaimFromToken(token, Claims::getSubject));
	}

	@Override
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	// for retrieving any information from token we will need the secret key
	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	//check if the token has expired
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	@Override
	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims, userDetails.getUsername());
	}

	private String doGenerateToken(Map<String, Object> claims, String subject) {
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	@Override
	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token).orElseThrow(TokenNotFoundException::new);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}

	@Override
	public AuthenticationResponse generateNewAuthorization(User user) {
		LoginDTO authenticationRequest = new LoginDTO();
		authenticationRequest.setEmail(user.getEmail());
		authenticationRequest.setPassword(user.getPassword());
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
		final String token = this.generateToken(userDetails);
		return new AuthenticationResponse(token);
	}
}