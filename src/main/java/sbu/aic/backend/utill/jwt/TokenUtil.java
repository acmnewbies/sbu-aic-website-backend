package sbu.aic.backend.utill.jwt;


import org.springframework.security.core.userdetails.UserDetails;
import sbu.aic.backend.domains.authentication.AuthenticationResponse;
import sbu.aic.backend.models.User;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

public interface TokenUtil extends Serializable {

    /**
     * retrieve username from given token
     * @param token
     * @return
     */
    public Optional<String> getUsernameFromToken(String token);

    /**
     * retrieve expiration date from token
     * @param token
     * @return
     */
    public Date getExpirationDateFromToken(String token);

    /**
     * generate token for user
     * @param userDetails
     * @return
     */
    public String generateToken(UserDetails userDetails);

    /**
     * validate token
     * @param token
     * @param userDetails
     * @return
     */
    public Boolean validateToken(String token, UserDetails userDetails);

    public AuthenticationResponse generateNewAuthorization(User user);

}