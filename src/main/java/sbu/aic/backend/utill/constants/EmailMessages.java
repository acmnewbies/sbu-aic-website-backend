package sbu.aic.backend.utill.constants;

public class EmailMessages {
    public static final String CONFIRM_SUBJECT = "SBU AI Challenge Account Verification";
    public static final String CONFIRM_MESSAGE = "Click this link below to confirm your account:\n" + URLS.baseUrl + "confirmAccount?token=";
    public static final String CONFIRM_COMPLETE = "Your account has been confirmed!";

    public static final String RECOVER_SUBJECT = "SBU AI Challenge password recovery";
    public static final String RECOVER_MESSAGE = "Click this link below to reset your password:\n" + URLS.baseUrl + "forgot?token=";
    public static final String RECOVER_COMPLETE = "Your Password Changed";
}
