package sbu.aic.backend.utill.exceptions.NotFoundExceptions;


public class AuctionNotFoundException extends RuntimeException {
    public AuctionNotFoundException(Long id) {
        super("Auction with id " + id + " not found");
    }
}