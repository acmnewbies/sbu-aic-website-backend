package sbu.aic.backend.utill.exceptions;

public class FullAuctionException extends RuntimeException {
    public FullAuctionException() {
        super("auction is Full");
    }

}
