package sbu.aic.backend.utill.exceptions;

public class NotAllowedToLeaveAuctionException extends RuntimeException {
    public NotAllowedToLeaveAuctionException() {
        super("you are the top bidder,you can't leave the auction");
    }

}
