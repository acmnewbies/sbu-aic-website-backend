package sbu.aic.backend.utill.exceptions.NotFoundExceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String email) {
        super("User with email " + email + " not found");
    }
}