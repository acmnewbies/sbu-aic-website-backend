package sbu.aic.backend.utill.exceptions.IllegalInputExceptions;

public class IllegalAuctionInputException extends RuntimeException {
    public IllegalAuctionInputException() {
        super("Auction creation fields are invalid");
    }
}
