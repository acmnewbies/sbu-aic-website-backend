package sbu.aic.backend.utill.exceptions.IllegalInputExceptions;

public class IllegalUserInputException extends RuntimeException {
    public IllegalUserInputException() {
        super("Fields were invalid. Please check them out.");
    }
}